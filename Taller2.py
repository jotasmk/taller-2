# Ejercicio 1

def cantAparicionesSub(lista1,lista2):
    count=0
    for i in range(0,len(lista1)):  #Barre elementos de lista1
        stop = False  #Flag para dejar de barrer en caso de encontrar el elemento, o llegar al final de la lista2 
        j=0
        while stop == False and j < len(lista2):   #Barre elementos de lista2 y compara con el i-ésimo elemento de la lista 1       
            if lista1[i] == lista2[j]:
                count = count + 1                        	
                stop = True  #Si lo encuentra, frena
            if j == len(lista2)-1:
                stop = True #Si llega al final, frena
            j=j+1    
    return count       

a=[1,1,1,1,2]
b=[1,1,4,5,7,8]

print("Ejercicio 1. Ejemplo:")
print("Lista1:",a)
print("Lista2:",b)
print("cantAparicionesSub(Lista1,Lista2)=",cantAparicionesSub(a,b))

### Ejercio 2

def listaTriangular(lista):
    esTriangular = True #La lista es triangular hasta que se demuestre lo contrario
    stop = False
    i = 0
    while i < len(lista)-1 and stop == False:  #Barre todos los elementos de la lista menos el último, a menos que stop se vuelva True
        if lista[i] < lista[i+1]: #Revisa si el elemento i es menor que el siguiente, si da True, "por ahora es creciente" así que no descarta que pueda ser triangular
            if i < len(lista)-2: 
                i = i + 1
            else:
                esTriangular = False #Si el anteúltimo elemento es menor que el último, la lista no puede ser triangular, así que para y devuelve False
                stop = True
        if lista[i] == lista[i+1]:  #Si el elemento i es igual al siguiente, devuelve False
            esTriangular = False
            stop = True
        if lista[i] > lista[i+1]: #Caso en que el elemento i es mayor que el siguiente
            if i == 0: 
                esTriangular = False #Si lo anterior es True y i es el primer elemento, entonces la lista no puede ser Triangular y devuelve False
                stop = True
            else:
                for j in range(i,len(lista)-1): #Barre los elementos de la lista desde i hasta el anteúltimo
                    if lista[j] > lista[j+1]: #Mientras siga siendo decreciente, sigue
                        esTriangular = True
                        if j == len(lista)-2: 
                            stop = True #Para que no barra todos los i de nuevo en caso de ya ver que la lista es triangular
                        i = i + 1
                    else:
                        esTriangular = False #Deja de ser decreciente, así que para y devuelve False
                        stop = True
    return esTriangular

a=[1,2,1,0]
b=[1,2,4,6,77,8,4,-1,0]
c=[1,1,3,4,2,0]
d=[4,3,2,1]
e=[1,2,3,4]
f=[1,2,2,3,4,3,2,1]
g=[1,2,3,2,1,1]



print("Ejercicio 2. Ejemplos:")
print("Lista1:",a)
print("Lista2:",b)
print("Lista3:",c)
print("Lista4:",d)
print("Lista5:",e)
print("Lista6:",f)
print("Lista7:",g)

print("listaTriangular(Lista1)=",listaTriangular(a))
print("listaTriangular(Lista2)=",listaTriangular(b))
print("listaTriangular(Lista3)=",listaTriangular(c))
print("listaTriangular(Lista4)=",listaTriangular(d))
print("listaTriangular(Lista5)=",listaTriangular(e))
print("listaTriangular(Lista6)=",listaTriangular(f))
print("listaTriangular(Lista7)=",listaTriangular(g))

